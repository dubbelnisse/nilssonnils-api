var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');

gulp.task('scripts', function() {
    gulp.src(['./bower_components/angular/angular.js', './bower_components/moment/moment.js', './src/javascript/showsearch.js'])
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./public/javascript/'));
});

gulp.task('sass', function () {
  gulp.src('./src/**/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['ie >= 9', 'last 2 versions']
    }))
    .pipe(gulp.dest('./public'));
});

gulp.task('watch', function () {
  gulp.watch('./src/**/*.js', ['scripts']);
  gulp.watch('./src/**/*.scss', ['sass']);
});

gulp.task('default', [
  'scripts',
  'sass',
  'watch'
]);