var express = require('express');
var router = express.Router();
var request = require('request');
var moment = require('moment');

//Trak api url
var traktShow = 'http://api.trakt.tv/user/calendar/shows.json/17072535f86416f24e6f50798dc1fb78/dubbelnisse/' + moment().subtract(1, 'd').format('YYYYMMDD');

//GET shows
router.get('/', function(req, res) {
  //Get data
  request(traktShow, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      //Set variables
      var data = JSON.parse(body);
      var datesTest = [];

      //Setup a test dates array
      for(var i = 0; i < 7; i++) {
        datesTest.push(moment().add(i, 'd').format('YYYYMMDD'));
      }
      
      //Repalce the dates to suite sweden  
      for(var i = 0; i < data.length; i++) {
        //Get response date and add one day with momentjs
        var newDate = moment(data[i].date).add(1, 'd').format('YYYYMMDD');
        //Replace default date with new
        data[i].date = newDate;
      }

      //Compare trakt dates with upcoming week dates
      function addMissingDays (trakt, days) {
        var dates = trakt
          .map(function (day) {
            return day.date;
          });
        
        days
          .filter(function (date) {
            return dates.indexOf(date) === -1;
          })
          .map(function (date) {
            trakt.push({
              date: date,
              episodes: ['Nothing today']
            });
          });

        trakt
          .sort(function (a, b) {
            return a.date - b.date;
          })

        return trakt;
      }

      //Sort and add missig dates to result
      data = addMissingDays(data, datesTest);

      res.send(data);
    }
  });

});

module.exports = router;
