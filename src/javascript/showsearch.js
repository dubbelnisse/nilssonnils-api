angular.module('showSearch', [])
.controller('search', function($scope, $http) {

  $scope.tvSearch = function() {

    var query = document.getElementById('searchInput').value;
    var apiKey = '27cfec6c9eb8080cb7d8025ba420e2d7';

    if (query.length > 0) {
      $http
      .get('http://api.themoviedb.org/3/search/tv?query=' + query + '&api_key=' + apiKey)
      .success(function(data) {

        data.results.forEach( function(d) {
          if (d.first_air_date != null) {
            var newDate = moment(d.first_air_date).format('YYYY');
            d.first_air_date = newDate;
          }
        });

        $scope.results = data.results
      });
    } else {
      $scope.results = '';
    }
  };

  $scope.reset = function() {
    document.getElementById('searchInput').value = '';
    $scope.tvSearch();
  }

});